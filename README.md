# Skeleton python+pandas+flask app for testing on Bluemix.
This is a toy application to demonstrate encapsulating
an algorithm written using python/pandas into a web service,
and deploying it on IBM Bluemix.

The algorithm is trivial - it just transposes a matrix
of integer values supplied in the POST body.

Example usage:
```sh
$ curl -d '{ "data": [ [1, 2, 3 ], [4, 5, 6] ] }' -H 'Content-Type:application/json' https://blue-pandas-flask.eu-gb.mybluemix.net/transpose
```

Produces the output
```
{"result": [[1, 4], [2, 5], [3, 6]]}
```

## Useful links

- [Manifests](https://docs.cloudfoundry.org/devguide/deploy-apps/manifest.html) 
- [Build packs](https://console.ng.bluemix.net/catalog/)
- [Python buildpack](https://github.com/cloudfoundry/python-buildpack)

## Notes on code

- Web service based on [flask](http://flask.pocoo.org/docs/0.12/)
- Python virtual environment under ./venv
- Visual studio code tasks, see ./vscode/settings.json

### Key files for bluemix deployment
There are three files that are important when deploying to Bluemix:

The [Procfile](Procfile) specifies the process to run, in our case:
```
web: python app.py
```

The [manifest.yml](manifest.yml) specifies the target to deploy to, how much memory to give it and so on:
```
applications:
- path: .
  memory: 128M
  instances: 1
  domain: eu-gb.mybluemix.net
  name: blue-pandas-flask
  host: blue-pandas-flask
  disk_quota: 1024M
```

The [.cfignore](.cfignore) file specifies files which should be ignored
when you do a `cf push` to the cloud (e.g. READMEs, local environment).

## Running locally
Before pushing to the bluemix cloud, try running locally:

### Create virtual environment
```
$ virtualenv -p python3.5 venv
```

### Install packages
```
# Packages needed at runtime (e.g. flask, pandas)
$ venv/bin/pip install -r requirements.txt

# Packages needed only for development purposes (e.g. lint)
$ venv/bin/pip install -r requirements_dev.txt
```

### Run server
```
$ venv/bin/python3.5 app.py
```

Or, in Visual Studio code, just hit `Ctrl-Shift-B`
(see the .vscode/tasks.json file)

The application listens on port 8080, binding to all interfaces
(can override the port using the `PORT` environment variable.)

### Test, using curl
```
$ curl -v -d '{ "data": [ [1, 2, 3 ], [4, 5, 6] ] }' \
       -H 'Content-Type:application/json' \
       localhost:8080/transpose
```

Response:
```
{"result": [[1, 4], [2, 5], [3, 6]]}
```

## Deploy to bluemix
Now it works locally, let's deploy to Bluemix

### Create account
Bluemix offer a "free one month trial" (conditions apply).

- Go to the [registration page](https://console.ng.bluemix.net/registration)
- Enter details
- Submit
- Should get an email (check Junk folder, also you may have to cut-paste the link into browser rather than directly clicking on it in Outlook)
- Click the link

### Create Organisation/Space
- Login
- Create Organisation
  - Region: United kingdom
  - Name: "blue-pandas" :-)
  - Click Create
- Create Space: "dev"
  > Spaces help you manage access and permissions for a set of resources
  > and map nicely to development stages like dev, test, and prod.
- Click "I'm ready"

### Create Starter App

On [daskboard](https://console.eu-gb.bluemix.net/dashboard/apps/)
click "create app"

- Ignore the "Compute" options (as will require upgrading from the free tier)
- Similarly, ignore  "Storage", "Network", "Security" options
- Scroll down to "Cloud Foundary Apps" and choose "Python"

On "Create a Cloud Foundary App" screen:
- App name: blue-pandas-flask
- Hostname: blue-pandas-flask
- Domain: eu-gb.mybluemix.net

Accept the default pricing plan

> Congratulations, you deployed a Hello World sample application on IBM® Bluemix®!

You could click on the "Download the sample code" link to get a zip-file
containing the source code - but this is optional, it should be possible
to just push the "blue-pandas-flask" code up in its place.

To check it's working, go to

https://blue-pandas-flask.eu-gb.mybluemix.net/

Yup, it's working :-)

## Install Cloud Foundary CLI
Instructions [here](https://github.com/cloudfoundry/cli#downloads)

In my case:
```
$ mkdir -p /tmp/xxx && cd /tmp/xxx
$ curl -L "https://cli.run.pivotal.io/stable?release=linux64-binary&source=github" | tar -zx
$ sudo mv cf /usr/local/bin/
$ cf --version

cf version 6.25.0+787326d.2017-02-28
```

## Udpate manifest.yml
Edit the file and edit these lines
``` 
  domain: eu-gb.mybluemix.net
  name: blue-pandas-flask
  host: blue-pandas-flask
``` 
to match your setup.

## cf api
Set the API endpoint (per region you set up your account in)

```
$ cf api https://api.eu-gb.bluemix.net

Setting api endpoint to https://api.eu-gb.bluemix.net...
OK

API endpoint:   https://api.eu-gb.bluemix.net
API version:    2.54.0
Not logged in. Use 'cf login' to log in.
```
## cf login
Authenticate:
```
$ cf login

API endpoint: https://api.eu-gb.bluemix.net

Email> mark.warren@bjss.com
Password> 

    ...
    Targeted org blue-pandas
    Targeted space dev

    API endpoint:   https://api.eu-gb.bluemix.net (API version: 2.54.0)
    User:           mark.warren@bjss.com
    Org:            blue-pandas
    Space:          dev
```
## cf push
Push the application
```
$ cf push
```
This quite a bit of output:
```
$ cf push
Using manifest file /home/mark/Desktop/bluemix-pandas-flask/manifest.yml

Updating app blue-pandas-flask in org blue-pandas / space dev as mark.warren@bjss.com...
OK

Using route blue-pandas-flask.eu-gb.mybluemix.net
Uploading blue-pandas-flask...
Uploading app files from: /home/mark/Desktop/bluemix-pandas-flask
Uploading 3.8K, 13 files
Done uploading               
OK

Stopping app blue-pandas-flask in org blue-pandas / space dev as mark.warren@bjss.com...
OK

Starting app blue-pandas-flask in org blue-pandas / space dev as mark.warren@bjss.com...
Downloading swift_buildpack_v2_0_4-20170125-2344...
Downloading binary_buildpack...
Downloading dotnet-core_v1_0_1-20161005-1225...
Downloading liberty-for-java_v3_4_1-20161030-2241...
Downloading xpages_buildpack_v1_2_1-20160913-103...
Downloaded swift_buildpack_v2_0_4-20170125-2344
Downloading dotnet-core...
Downloaded xpages_buildpack_v1_2_1-20160913-103
Downloaded binary_buildpack
Downloading liberty-for-java...
Downloaded liberty-for-java_v3_4_1-20161030-2241
Downloading sdk-for-nodejs...
Downloaded dotnet-core_v1_0_1-20161005-1225
Downloading php_buildpack...
Downloading python_buildpack...
Downloaded dotnet-core
Downloading sdk-for-nodejs_v3_10-20170119-1146...
Downloaded liberty-for-java
Downloading liberty-for-java_v3_7-20170118-2046...
Downloaded sdk-for-nodejs
Downloading xpages_buildpack...
Downloaded php_buildpack
Downloading java_buildpack...
Downloaded sdk-for-nodejs_v3_10-20170119-1146
Downloading swift_buildpack...
Downloaded python_buildpack
Downloading ruby_buildpack...
Downloaded liberty-for-java_v3_7-20170118-2046
Downloading nodejs_buildpack...
Downloaded xpages_buildpack
Downloading staticfile_buildpack...
Downloaded java_buildpack
Downloading go_buildpack...
Downloaded swift_buildpack
Downloaded ruby_buildpack
Downloaded nodejs_buildpack
Downloaded staticfile_buildpack
Downloaded go_buildpack
Creating container
Successfully created container
Downloading app package...
Downloaded app package (3.7K)
Downloading build artifacts cache...
Downloaded build artifacts cache (68M)
Staging...
-------> Buildpack version 1.5.5
     $ pip install -r requirements.txt
You are using pip version 8.1.1, however version 9.0.1 is available.
You should consider upgrading via the 'pip install --upgrade pip' command.
DEPRECATION: --allow-all-external has been deprecated and will be removed in the future. Due to changes in the repository protocol, it no longer has any effect.
You are using pip version 8.1.1, however version 9.0.1 is available.
You should consider upgrading via the 'pip install --upgrade pip' command.
Exit status 0
Staging complete
Uploading droplet, build artifacts cache...
Uploading build artifacts cache...
Uploading droplet...
Uploaded build artifacts cache (67.9M)
Uploaded droplet (67.9M)
Uploading complete
Destroying container
Successfully destroyed container

0 of 1 instances running, 1 starting
0 of 1 instances running, 1 starting
1 of 1 instances running

App started


OK

App blue-pandas-flask was started using this command `python app.py`

Showing health and status for app blue-pandas-flask in org blue-pandas / space dev as mark.warren@bjss.com...
OK

requested state: started
instances: 1/1
usage: 128M x 1 instances
urls: blue-pandas-flask.eu-gb.mybluemix.net
last uploaded: Thu Apr 6 12:34:10 UTC 2017
stack: cflinuxfs2
buildpack: python 1.5.5

     state     since                    cpu    memory          disk         details
#0   running   2017-04-06 01:35:46 PM   0.2%   47.7M of 128M   272M of 1G
```
## Environment Variables
For this example, there are no relevant environment variables.

But normally these would be needed: there are `cf` commands to view/set them
###  View environment variables
```
$ cf env blue-pandas-flask
```

The output looks like this:
```
Getting env variables for app blue-pandas-flask in org blue-pandas / space dev as mark.warren@bjss.com...
OK

System-Provided:


{
 "VCAP_APPLICATION": {
  "application_id": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
  "application_name": "blue-pandas-flask",
  "application_uris": [
   "blue-pandas-flask.eu-gb.mybluemix.net"
  ],
  "application_version": "vvvvvvvv-vvvv-vvvv-vvvv-vvvvvvvvvvvv",
  "limits": {
   "disk": 1024,
   "fds": 16384,
   "mem": 128
  },
  "name": "blue-pandas-flask",
  "space_id": "zzzzzzzz-zzzz-zzzz-zzzz-zzzzzzzzzzzz",
  "space_name": "dev",
  "uris": [
   "blue-pandas-flask.eu-gb.mybluemix.net"
  ],
  "users": null,
  "version": "vvvvvvvv-vvvv-vvvv-vvvv-vvvvvvvvvvvv"
 }
}

No user-defined env variables have been set
```

### Set FOOBAR environment variable
To set and check values:
```
$ cf set-env blue-pandas-flask FOOBAR xxx
$ cf get blue-pandas-flask

    User-Provided:
    FOOBAR: xxx
```

Need to restage for changes to take effect:
```
$ cf restage blue-pandas-flask
```
