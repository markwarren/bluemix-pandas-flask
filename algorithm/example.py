'''
Examples of algorithms for data transformation
'''

import pandas as pd

def transpose(input_df):
    '''
    Trivial transformation function which
    - expects a data frame as input
    - returns its transpose as output
    '''
    assert isinstance(input_df, pd.DataFrame)
    output_df = input_df.T
    return output_df
