'''
Flask based web application
'''

import atexit
import os
import sys

import cf_deployment_tracker
from flask import Flask, request

import algorithm.example

# Emit Bluemix deployment event
cf_deployment_tracker.track()

FLASK_APP = Flask(__name__)

# On Bluemix, get the port number from the environment variable PORT
# When running this app on the local machine, default the port to 8080
FLASK_PORT = int(os.getenv('PORT', 8080))

@atexit.register
def shutdown():
    '''
    Exit handler
    '''
    pass
    ### if client:
    ###     client.disconnect()

@FLASK_APP.route('/')
def hello_world():
    '''
    Trivial flask endpoint
    '''
    return 'Hello, World! Python version is {0}'.format(sys.version)

@FLASK_APP.route('/transpose', methods=['POST'])
def transpose():
    '''
    Main web API endpoint
    '''
    FLASK_APP.logger.info("JSON: {0}".format(request.json))
    if request.json is None:
        return '{ "error": "expected JSON" }', 400

    try:
        data = request.json['data']
    except KeyError:
        return '{ "error": "missing argument" }', 400

    # Convert JSON request to a DataFrame
    from pandas import DataFrame
    input_df = DataFrame(data)

    # Perform calculation
    output_df = algorithm.example.transpose(input_df)

    # Convert DataFrame to JSON and return as successful response
    import json
    body = json.dumps({'result': output_df.as_matrix().tolist()}) + "\n"
    return body

if __name__ == '__main__':
    FLASK_APP.run(host='0.0.0.0', port=FLASK_PORT, debug=False)
